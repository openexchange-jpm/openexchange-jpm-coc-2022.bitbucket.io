document.addEventListener('DOMContentLoaded', function() {
    var pageTop = document.getElementById("page-top");
    if (pageTop !== null) {

        // modal box javascript 
        // get all listitems
        var titleTileLists = document.querySelectorAll(".tile-title");

        var changeModalboxTitle = function(){
            var postMetadataTitle = document.getElementById("postMetadataTitle");
            postMetadataTitle.style.display = "none";
            setTimeout(() => {
                var modalBoxTitle = document.querySelector(".post-details-title");  
                var newModalTitle = modalBoxTitle.innerHTML.replace('SAST - ','SAST<br>');  
                modalBoxTitle.innerHTML = newModalTitle;
                postMetadataTitle.style.display = "block";
            }, 400);
     
            // alert(modalBoxTitle.innerHTML);      

        }

        for (var i = 0; i < titleTileLists.length; i++) {
            titleTileLists[i].addEventListener('click', changeModalboxTitle, false);
        }

        // end modal box javascript 

        // feature knovio javascript
        var featureKnovioDes = document.querySelector(".featured-knovio-description");
        var featureKnovioDesHTML = decodeHTML(featureKnovioDes.innerHTML);
        console.log(featureKnovioDes.innerHTML);
        console.log(decodeHTML(featureKnovioDes.innerHTML))
        featureKnovioDes.innerHTML = featureKnovioDesHTML;

        // end feature knovio javascript
        
    }
});

function decodeHTML(str) {
    var txt = document.createElement('textarea');
	txt.innerHTML = str;
	return txt.value;
}
